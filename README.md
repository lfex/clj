# clj

*Clojure functions and macros for LFE*

<img src="resources/images/clojure.png" />

## Introduction

The clj library offers a collection of functions and macros that you may find useful
or enjoyable in LFE if you've come from a Clojure background.

This library is in the process of extracting the Clojure functionality that made its
way into the [lutil]() LFE library


## Installation

In your ``rebar.config`` file, update your ``deps`` section to include
``clj``:

```erlang
{deps, [
  ...
  {clj, ".*", {git, "git://github.com/lfex/clj.git"}},
  ...
]}
```


## Usage

TBD

(We are changing the functions and macros from what they were in lutil, so usage has
not yet stabilized. If you want to be an alpha tester/user, be sure to look at the
unit tests and code comments, both in the modules and in the include files.)
